const express = require('express');
const bodyParser = require('body-parser');
let app = express();
app.use(bodyParser.json({limit: '1mb'}));
app.use(bodyParser.urlencoded({limit: '1mb', extended: true}));


app
    .route('/jetpacks/:id?')
    .get(require('./controller/Jetpack/GetJetpackController'))
    .post(require('./controller/Jetpack/CreateJetpackController'));

app
    .route('/bookings')
    .post(require('./controller/Reservation/CreateReservationController'));

app
    .route('/availabilities')
    .get(require('./controller/Availabilities/GetAvailabilitiesController'));

app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
});