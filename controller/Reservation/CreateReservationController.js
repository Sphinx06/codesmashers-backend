const Reservation = require("../../src/Entity/Reservation");
const uuidv4 = require('uuid/v4');
const db = require('../../src/Db');
const ReservationsRepository = require('../../src/Repository/ReservationRepository');
const Moment = require('moment/moment');

module.exports = (req, res) => {
    console.log(req.body);
    let reservation = new Reservation();
    reservation.id = uuidv4();
    reservation.jetpack_id = req.body.jetpack_id;

    res.header("Access-Control-Allow-Origin", "*");
    try {
        reservation.start_date = Moment(req.body.start_date);
        reservation.end_date = Moment(req.body.end_date);
        let repository = new ReservationsRepository(db);
        repository.create(reservation);

        res.status(201).send(reservation.toJson())
    } catch(error) {
        res.status(400).send({error:error.toString()})
    }
};