const Repository = require('../../src/Repository/JetpackRepository');
const db = require('../../src/Db.js');
module.exports = (req, res) => {
  let monrepo = new Repository(db);
  const jetpacks = monrepo.getAll();
  console.log(jetpacks)
  res.header('Access-Control-Allow-Origin', '*');
  res.status(201).send(jetpacks);
};
