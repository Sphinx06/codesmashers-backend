const Repository = require('../../src/Repository/AvailabilityRepository');
const db = require('../../src/Db.js');
module.exports = (req, res) => {
    let repo = new Repository(db);
    const availabilities = repo.getAll();
    console.log(availabilities);
    res.header('Access-Control-Allow-Origin', '*');
    res.status(201).send(availabilities);
};