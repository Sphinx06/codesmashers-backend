module.exports = function(config) {
  config.set({
    mutator: "javascript",
    packageManager: "npm",
    reporters: ["html", "clear-text", "progress", "dashboard"],
    testRunner: "jest",
    transpilers: [],
    coverageAnalysis: "off",
    thresholds: {
      break: 70
      // ..
    },
    files:[
        'src/**/*.js', '!src/fill-db.js', '!src/test-script.js', '!src/Db.js'
    ]
  });
};
