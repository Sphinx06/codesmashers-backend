const Repository = require('./Repository/JetpackRepository');
const ReservationRepo = require('./Repository/ReservationRepository');
const db = require('./Db')

const repository = new Repository(db);
const reservationRepo = new ReservationRepo(db)

function randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

Array(5).fill().forEach((_, i) => {
    repository.save({
        id : i,
        name: Math.random().toString(36).substring(7),
        image: Math.round(Math.random() * 1000) / 100,
    });

    reservationRepo.create({
        jetpack_id : i,

    })
});