const Reservation = require('./Reservation');
const Moment = require('moment-timezone');
describe('Reservation toJson', function () {
    test('Test toJson', () => {
        let reservation = new Reservation();
        reservation.id = '1';
        reservation.jetpack_id = '1234567890';
        reservation.end_date = Moment().add(3, "d");
        reservation.start_date = Moment();
        expect(reservation.toJson()).toMatchObject({
            id: '1',
            jetpack_id: '1234567890',
            end_date: Moment().add(3, "d").format(),
            start_date: Moment().format()
        })
    })

    test('Wrong date',()=> {
        let reservation = new Reservation();
        expect(() => reservation.start_date = "REQ").toThrow("Parameters is not a valid Moment object")
        expect(() => reservation.end_date = Moment("aa")).toThrow("Parameters is not a valid Moment object")
    })
});
