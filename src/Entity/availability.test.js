const Availability = require('./Availability');
const Interval = require("../Util/interval");
const Moment = require("moment-timezone");
describe('Exclude availability', function(){
    test('Remove a Moment interval', () => {
        let availability = new Availability();
        availability.exclude_reservation(
            Moment().add(1, 'd'),
            Moment().add(2, 'd')
        );
        expect(availability.avalaibilities.length).toBe(2);
        availability.exclude_reservation(
            Moment().add(10, 'd'),
            Moment().add(12, 'd')
        );
        expect(availability.avalaibilities.length).toBe(3)

    })
});

describe('Format availability', function(){
    test('Availability as list of dict', () => {
        let availability = new Availability();
        availability.jetpack_id = "1234567890";

        availability.exclude_reservation(
            Moment().add(4, 'd'),
            Moment().add(8, 'd')
        );
        expect(availability.formatAvailability().length).toBe(2);
        expect(availability.formatAvailability()).toMatchObject([
            {
                jetpack_id: '1234567890',
                end_date: Moment()
                    .add(4, 'd').format(),
                start_date: Moment().format()
            },
            {
                jetpack_id: '1234567890',
                end_date: Moment()
                    .add(2, 'y').format(),
                start_date: Moment()
                    .add(8, 'd').format()
            }
        ])
    })
});
