const Moment = require("moment-timezone");
const Interval = require("../Util/interval.js");
module.exports = class {
    constructor () {
        this._jetpack_id = null;
        this._avalaibilities = [new Interval(Moment(), Moment().add(2, 'y'))];
    }

    exclude_reservation(start_date, end_date) {
        let li = [];
        this.avalaibilities.forEach(function(elem) {
            if (elem.includes(new Interval(start_date, end_date))) {
                li = li.concat(elem.exclusion(new Interval(start_date, end_date)))
            } else {
                li.push(elem)
            }
        });
        this.avalaibilities = li;
        return this
    }

    get jetpack_id() {
        return this._jetpack_id;
    }

    set jetpack_id(value) {
        this._jetpack_id = value;
    }


    get avalaibilities() {
        return this._avalaibilities;
    }

    set avalaibilities(value) {
        this._avalaibilities = value;
    }

    formatAvailability () {
        let ListBuilder = [];
        let jetpack_id = this.jetpack_id;
        this.avalaibilities.forEach(function(elem) {
            ListBuilder.push({
                jetpack_id: jetpack_id,
                end_date: Moment(elem.end).format(),
                start_date: Moment(elem.start).format()
            })
        });
        return ListBuilder
    }
};
