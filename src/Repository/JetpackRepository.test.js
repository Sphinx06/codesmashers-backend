const Repository = require('./JetpackRepository')
const Jetpack = require('../Entity/Jetpack')
describe('Jetpack repository Create', function () {
  test('Save a jetpack', () => {
    const dbMock = {
      get: jest.fn(),
      push: jest.fn(),
      write: jest.fn()
    }
    dbMock.get.mockReturnValue(dbMock)
    dbMock.push.mockReturnValue(dbMock)

    const repository = new Repository(dbMock)
    repository.create({ id: 1, name: 'Unit test' })
    expect(dbMock.write.mock.calls.length).toBe(1)
  })
  test('Save a jetpack empty object', () => {

    const repository = new Repository({})
    expect(() => repository.create()).toThrow("Jetpack object is undefined")

  })
  test('Save a jetpack no name', () => {

    const repository = new Repository({})
    expect(() => repository.create(new Jetpack())).toThrow("Jetpack object is missing information")

  })
})
describe('Jetpack repository Save', function () {

  test('Save a jetpack', () => {
    const dbMock = {
      get: jest.fn(),
      push: jest.fn(),
      write: jest.fn()
    }
    dbMock.get.mockReturnValue(dbMock)
    dbMock.push.mockReturnValue(dbMock)

    const repository = new Repository(dbMock)
    repository.save({ id: 1, name: 'Unit test' })
    expect(dbMock.write.mock.calls.length).toBe(1)
  })
})
describe('GetAll', () => {
  test('get all jetpacks', () => {
    let dbMock = {
      get: jest.fn(),
      size: jest.fn(),
      value: jest.fn()
    }
    const jetPack = new Jetpack()
    jetPack.id = 1
    jetPack.name = 'jet'
    jetPack.image = '123'

    dbMock.get.mockReturnValue(dbMock)
    dbMock.value.mockReturnValue([
      jetPack
    ])
    const repository = new Repository(dbMock)
    let jetpacks = repository.getAll()
    console.log(jetpacks)
    expect(jetpacks.length).toBe(1)
    expect(jetpacks[0].name).toBe('jet')
  })
})
