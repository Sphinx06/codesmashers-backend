const Repository = require("./AvailabilityRepository.js");
const Moment = require("moment-timezone");

describe('Get availability', function () {
    test('Get all availabilities', () => {
        let dbMockReservations = {
            get: jest.fn(),
            value: jest.fn()
        };
        dbMockReservations.get.mockReturnValue(dbMockReservations);
        dbMockReservations.value.mockReturnValue([
            {
                "_id": "24435ef1-f236-4382-863a-59ad0da90887",
                "_jetpack_id": "54b1fcac-8692-45d6-9e5e-cebc5598b40d",
                "_start_date": Moment().add("1", "d").format(),
                "_end_date": Moment().add("3", "d").format()
            },
            {
                "_id": "1c0893dc-c86f-42a8-92c7-e8b1a963fadd",
                "_jetpack_id": "54b1fcac-8692-45d6-9e5e-cebc5598b40d",
                "_start_date": Moment().add("5", "d").format(),
                "_end_date": Moment().add("8", "d").format(),
            },
            {
                "_id": "be9fa5e6-12c3-4fd0-8dca-582267dd5166",
                "_jetpack_id": "9331f5a5-dd79-4af1-9a1a-7bc835d5fb6d",
                "_start_date": Moment().add("1", "d").format(),
                "_end_date": Moment().add("2", "d").format()
            }
        ]);


        let dbMockJetpacks = {
            get: jest.fn(),
            value: jest.fn()
        };
        dbMockJetpacks.get.mockReturnValue(dbMockJetpacks);
        dbMockJetpacks.value.mockReturnValue([
            {
                "_id": "54b1fcac-8692-45d6-9e5e-cebc5598b40d",
                "_name": "Jet Pack à fusion X98371",
                "_image": "Base 64 ..."
            },
            {
                "_id": "9331f5a5-dd79-4af1-9a1a-7bc835d5fb6d",
                "_name": "Jet Pack à fusion X98371",
                "_image": "Base 64 ..."
            }
        ]);
        let repository = new Repository({});
        let availabilities = repository.getAll(dbMockReservations, dbMockJetpacks);
        expect(availabilities.length).toBe(5)
        expect(Moment(availabilities[0]._start_date).day()).toBe(Moment().day())
    });

    test('Get all availabilities, no jetpack in base', () => {
        let dbMockReservations = {
            get: jest.fn(),
            value: jest.fn()
        };
        dbMockReservations.get.mockReturnValue(dbMockReservations);
        dbMockReservations.value.mockReturnValue([
            {
                "_id": "24435ef1-f236-4382-863a-59ad0da90887",
                "_jetpack_id": "54b1fcac-8692-45d6-9e5e-cebc5598b40d",
                "_start_date": Moment().add("1", "d").format(),
                "_end_date": Moment().add("3", "d").format()
            },
            {
                "_id": "1c0893dc-c86f-42a8-92c7-e8b1a963fadd",
                "_jetpack_id": "54b1fcac-8692-45d6-9e5e-cebc5598b40d",
                "_start_date": Moment().add("5", "d").format(),
                "_end_date": Moment().add("8", "d").format(),
            },
            {
                "_id": "be9fa5e6-12c3-4fd0-8dca-582267dd5166",
                "_jetpack_id": "9331f5a5-dd79-4af1-9a1a-7bc835d5fb6d",
                "_start_date": Moment().add("1", "d").format(),
                "_end_date": Moment().add("2", "d").format()
            }
        ]);


        let dbMockJetpacks = {
            get: jest.fn(),
            value: jest.fn()
        };
        dbMockJetpacks.get.mockReturnValue(dbMockJetpacks);
        dbMockJetpacks.value.mockReturnValue([]);
        let repository = new Repository({});
        let availabilities = repository.getAll(dbMockReservations, dbMockJetpacks);
        expect(availabilities.length).toBe(0)
    });

    test('Get all availabilities, no reservation in base', () => {
        let dbMockReservations = {
            get: jest.fn(),
            value: jest.fn()
        };
        dbMockReservations.get.mockReturnValue(dbMockReservations);
        dbMockReservations.value.mockReturnValue([]);


        let dbMockJetpacks = {
            get: jest.fn(),
            value: jest.fn()
        };
        dbMockJetpacks.get.mockReturnValue(dbMockJetpacks);
        dbMockJetpacks.value.mockReturnValue([
                {
                    "_id": "54b1fcac-8692-45d6-9e5e-cebc5598b40d",
                    "_name": "Jet Pack à fusion X98371",
                    "_image": "Base 64 ..."
                },
                {
                    "_id": "9331f5a5-dd79-4af1-9a1a-7bc835d5fb6d",
                    "_name": "Jet Pack à fusion X98371",
                    "_image": "Base 64 ..."
                }
            ]);
        let repository = new Repository({});
        let availabilities = repository.getAll(dbMockReservations, dbMockJetpacks);
        expect(availabilities.length).toBe(2)
    });

    test('Get all availabilities, no data in db', () => {
        let dbMock = {
            get: jest.fn(),
            value: jest.fn()
        };
        dbMock.get.mockReturnValue(dbMock);
        dbMock.value.mockReturnValue([]);
        let repository = new Repository(dbMock);
        let availabilities = repository.getAll();
        expect(availabilities.length).toBe(0)
    });
});