module.exports = class {
    constructor (db) {
        this.db = db
    }

    create (reservation) {
        if (!reservation) {
            throw 'Reservation object is undefined'
        }

        if (!reservation.jetpack_id || !reservation.start_date || !reservation.end_date) {
            throw new Error('Reservation object is missing information')
        }

        this.db
            .get('reservations')
            .push(reservation)
            .write()
    }

    getAll() {
        return this.db.get('reservations').value()
    }

    getJetpackReservation(jetpack) {
        return this.db.get('reservations').filter({jetpack_id:jetpack}).value()
    }
}
