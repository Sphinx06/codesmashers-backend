const Repository = require('./ReservationRepository');
const Reservation = require('../Entity/Reservation');
const Moment = require('moment-timezone');
describe('Reservation repository Create', function () {
    test('Save a reservation', () => {
        const dbMock = {
            get: jest.fn(),
            push: jest.fn(),
            write: jest.fn()
        };
        dbMock.get.mockReturnValue(dbMock);
        dbMock.push.mockReturnValue(dbMock);

        const repository = new Repository(dbMock);

        let reservation = new Reservation();
        reservation.id = '1';
        reservation.jetpack_id = '1234567890';
        reservation.end_date = Moment().add(3, "d");
        reservation.start_date = Moment();

        repository.create(reservation);
        expect(dbMock.write.mock.calls.length).toBe(1)
    });
    test('Save a reservation empty object', () => {

        const repository = new Repository({});
        expect(() => repository.create()).toThrow("Reservation object is undefined")

    });
    test('Save a reservation no name', () => {

        const repository = new Repository({});
        expect(() => repository.create(new Reservation())).toThrow("Reservation object is missing information")

    })
});

describe('GetAll', () => {
    test('Get all reservations', () => {
        let dbMock = {
            get: jest.fn(),
            value: jest.fn()
        };

        let reservation = new Reservation();
        reservation.id = '1';
        reservation.jetpack_id = '1234567890';
        reservation.end_date = Moment().add(3, "d");
        reservation.start_date = Moment();

        dbMock.get.mockReturnValue(dbMock);
        dbMock.value.mockReturnValue([
            reservation
        ]);
        const repository = new Repository(dbMock);
        let reservations = repository.getAll();
        expect(reservations.length).toBe(1);
        expect(reservations[0].id).toBe('1');
        expect(reservations[0].jetpack_id).toBe('1234567890');
    })
});

describe('Get specific jetpack', () => {
    test('Get with specific jetpack and reservation exist', () => {
        let dbMock = {
            get: jest.fn(),
            filter: jest.fn(),
            value: jest.fn()
        };

        let reservation = new Reservation();
        reservation.id = '1';
        reservation.jetpack_id = '1234567890';
        reservation.end_date = Moment().add(3, "d");
        reservation.start_date = Moment();

        dbMock.get.mockReturnValue(dbMock);
        dbMock.filter.mockReturnValue(dbMock);
        dbMock.value.mockReturnValue([
            reservation
        ]);
        const repository = new Repository(dbMock);
        let reservations = repository.getJetpackReservation('1234567890');
        expect(reservations.length).toBe(1);
        expect(reservations[0].id).toBe('1');
        expect(reservations[0].jetpack_id).toBe('1234567890');
    });

    test('Get with specific jetpack and reservation doesn t exist', () => {
        let dbMock = {
            get: jest.fn(),
            filter: jest.fn(),
            value: jest.fn()
        };

        dbMock.get.mockReturnValue(dbMock);
        dbMock.filter.mockReturnValue(dbMock);
        dbMock.value.mockReturnValue([]);

        const repository = new Repository(dbMock);

        let reservations = repository.getJetpackReservation('1234567890');
        expect(reservations.length).toBe(0);
    });
});
