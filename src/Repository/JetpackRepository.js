const Jetpack = require("../Entity/Jetpack");

module.exports = class {
  constructor (db) {
    this.db = db
  }

  create (jetpack) {
    if (!jetpack) {
      throw 'Jetpack object is undefined'
    }

    if (!jetpack.id || !jetpack.name) {
      throw new Error('Jetpack object is missing information')
    }

    this.db
      .get('jetpacks')
      .push(jetpack)
      .write()
  }
    save (jetpack) {
        this.db
            .get('jetpacks')
            .push(jetpack)
            .write();
    }
  getAll () {
    let st = [];
    this.db.get('jetpacks').value().forEach(function(elem) {
      let jp = new Jetpack();
      jp.name = elem._name;
      jp.id = elem._id;
      jp.image = elem._image;
      st.push(jp.toJson())
    });
    return st;
  }
}
