const Reservation = require("./ReservationRepository");
const Jetpacks = require("./JetpackRepository")
const Availability = require("../Entity/Availability");
const Moment = require("moment-timezone");

module.exports = class {
    constructor (db) {
        this.db = db
    }

    getAll (dbReservations = this.db, dbJetpack = this.db) {
        let reservations = new Reservation(dbReservations).getAll();
        let jetpacks = new Jetpacks(dbJetpack).getAll();
        let availabilityDict = {};
        jetpacks.forEach(function(jetpack) {
            let avail = new Availability();
            avail.jetpack_id = jetpack.id;
            availabilityDict[jetpack.id] = avail;
        });
        reservations.forEach(function(elem) {
            if (elem._jetpack_id in availabilityDict){
                availabilityDict[elem._jetpack_id] = availabilityDict[elem._jetpack_id].exclude_reservation(
                    Moment(elem._start_date),
                    Moment(elem._end_date)
                );
            }
        });
        let StringBuilder = [];
        Object.values(availabilityDict).forEach(function(availability) {
            StringBuilder = StringBuilder.concat(availability.formatAvailability())
        });
        console.log(StringBuilder);
        return StringBuilder
    }
};
